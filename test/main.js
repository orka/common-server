/**
 * Filler test file
 * will run on test cmd
 */
'use strict';
const chai = require('chai');
const expect = chai.expect;
// const should = chai.should;
const assert = chai.assert;
const Server = require('./../src/main');


describe('When using main', () => {
    let server;
    //
    it('Should be defined', () => {
        expect(Server).to.not.be.undefined;
    });

    it('Should not throw when instance is created', () => {
        expect(() => { return new Server() }).to.not.throw();
    });

    it('Should be able to start', () => {
        server = new Server();
        return server.start()
            .then((__server) => {
                expect(__server.listening).to.be.true;
            })
    });

    it('Should be able to stop', () => {
        return server.stop()
            .then((__server) => {
                expect(__server.listening).to.be.false;
            })
    });
});
