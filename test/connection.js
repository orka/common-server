/**
 *
 * TResting request types and reuoonses
 * 
 */
'use strict';
const chai = require('chai');
const expect = chai.expect;
// const should = chai.should;
const assert = chai.assert;
//
const request = require('request-promise');
const Module = require('log-module');
Module.level = 'error';
//
const Server = require('./../src/main');
const config = require('./../src/config.json');
const path = '/test';
const headers = {
    'User-Agent': 'Common Server Test',
    "content-type": "application/json"
}
const hostname = 'localhost';
const protocol = 'http';
const url = `${protocol}://${hostname}:${config.server.port}${path}`;


describe('When making a successful GET request', () => {
    let server;
    let requestOptions = {
        method: 'GET',
        headers: headers,
        json: {},
        url: url
    };
    let data = { hello: 'Sam' }
    let successMessage = 'Success message';

    beforeEach(() => {
        server = new Server();

        return server.start()
            .then(() => {
                return server.route(path)
                    .get((__req) => {
                        server.respondSuccess(__req, data, successMessage);
                    });
            });
    });

    afterEach(() => {
        return server.stop();
    })

    //
    it('Should have a response.statusCode 200', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            });
    });
    //
    it('Should have a response.error false', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.error).to.equal(false);
            });
    });
    //
    it('Should have a response.path to equal path', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.path).to.equal(path);
            });
    });
    //
    it('Should have a response.hostname to equal hostname', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.hostname).to.equal(hostname);
            });
    });

    it('Should have a response.method to equal GET', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.method).to.equal('GET');
            });
    });

    it('Should have a response.message to equal successMessage', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.message).to.equal(successMessage);
            });
    });

    it('Should have a response.data have data', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.data.hello).to.equal(data.hello);
            });
    });

    it('Should have a response.performance defined', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.performance).to.be.a('string');
            });
    });
});




describe('When making a successful POST request', () => {
    let server;
    let data = { hello: 'Sam' };
    //
    let requestOptions = {
        method: 'POST',
        headers: headers,
        json: data,
        url: url
    };

    let successMessage = 'Success message';

    beforeEach(() => {
        server = new Server();

        return server.start()
            .then(() => {
                return server.route(path)
                    .post((__req) => {
                        server.respondSuccess(__req, __req.body, successMessage);
                    });
            });
    });

    afterEach(() => {
        return server.stop();
    })

    //
    it('Should have a response.statusCode 200', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            });
    });
    //
    it('Should have a response.error false', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.error).to.equal(false);
            });
    });
    //
    it('Should have a response.path to equal path', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.path).to.equal(path);
            });
    });
    //
    it('Should have a response.hostname to equal hostname', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.hostname).to.equal(hostname);
            });
    });

    it('Should have a response.method to equal POST', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.method).to.equal('POST');
            });
    });

    it('Should have a response.data have data', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.data.hello).to.equal(data.hello);
            });
    });
});




describe('When making a successful PUT request', () => {
    let server;
    let data = { hello: 'Sam' };
    //
    let requestOptions = {
        method: 'PUT',
        headers: headers,
        json: data,
        url: url
    };

    let successMessage = 'Success message';

    beforeEach(() => {
        server = new Server();

        return server.start()
            .then(() => {
                return server.route(path)
                    .put((__req) => {
                        server.respondSuccess(__req, __req.body, successMessage);
                    });
            });
    });

    afterEach(() => {
        return server.stop();
    })

    //
    it('Should have a response.statusCode 200', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            });
    });
    //
    it('Should have a response.error false', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.error).to.equal(false);
            });
    });
    //
    it('Should have a response.path to equal path', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.path).to.equal(path);
            });
    });
    //
    it('Should have a response.hostname to equal hostname', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.hostname).to.equal(hostname);
            });
    });

    it('Should have a response.method to equal PUT', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.method).to.equal('PUT');
            });
    });

    it('Should have a response.data have data', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.data.hello).to.equal(data.hello);
            });
    });
});




describe('When making a successful DELETE request', () => {
    let server;
    let data = { hello: 'Sam' };
    //
    let requestOptions = {
        method: 'DELETE',
        headers: headers,
        json: data,
        url: url
    };

    let successMessage = 'Success message';

    beforeEach(() => {
        server = new Server();

        return server.start()
            .then(() => {
                return server.route(path)
                    .delete((__req) => {
                        server.respondSuccess(__req, __req.body, successMessage);
                    });
            });
    });

    afterEach(() => {
        return server.stop();
    })

    //
    it('Should have a response.statusCode 200', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            });
    });
    //
    it('Should have a response.error false', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.error).to.equal(false);
            });
    });
    //
    it('Should have a response.path to equal path', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.path).to.equal(path);
            });
    });
    //
    it('Should have a response.hostname to equal hostname', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.hostname).to.equal(hostname);
            });
    });

    it('Should have a response.method to equal DELETE', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.method).to.equal('DELETE');
            });
    });

    it('Should have a response.data have data', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.data.hello).to.equal(data.hello);
            });
    });
});




describe('When making an error GET request', () => {
    let server;
    let requestOptions = {
        method: 'GET',
        headers: headers,
        json: {},
        url: url
    };
    let message = 'Error Message';

    beforeEach(() => {
        server = new Server();

        return server.start()
            .then(() => {
                return server.route(path)
                    .get((__req) => {
                        server.respondError(__req, message);
                    });
            });
    });

    afterEach(() => {
        return server.stop();
    })

    //
    it('Should have a response.statusCode 500', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            })
            .catch((__error) => {
                expect(__error.statusCode).to.equal(500);
            });
    });

    //
    it('Should have a response.error object defined', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            })
            .catch((__error) => {
                expect(__error.error).to.be.an('object');
            });
    });


    //
    it('Should have a response.error.path to equal path', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            })
            .catch((__error) => {
                expect(__error.error.path).to.equal(path);
            });
    });

    //
    it('Should have a response.error.hostname to equal hostname', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            })
            .catch((__error) => {
                expect(__error.error.hostname).to.equal(hostname);
            });
    });

    it('Should have a response.error.method to equal GET', () => {

        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            })
            .catch((__error) => {
                expect(__error.error.method).to.equal('GET');
            });
    });

    it('Should have a response.error.message have error message', () => {
        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            })
            .catch((__error) => {
                expect(__error.error.message).to.equal(message);
            });
    });


    it('Should have a response.error.performance defined', () => {

        return request(requestOptions)
            .then((__response) => {
                expect(__response.statusCode).to.equal(200);
            })
            .catch((__error) => {
                expect(__error.error.message).to.be.a('string');
            });
    });
});
