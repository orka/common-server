# Common Server

A common server app with a highly configurable features

### Purpose
Simple server instance startup Module

## Installation 

yarn
`yarn add ssh://git@bitbucket.uxserver.espial.com:7999/atg/common-server.git`

npm
`npm install ssh://git@bitbucket.uxserver.espial.com:7999/atg/common-server.git`

 

### Usage
```javascript
const CommonServer = require('common-server');
//This is a default configuration
const config = {
    "cors":{
        "origin": "*", //CORS policy
        "optionsSuccessStatus": 200 //For IE Browser status issues
    },
    "express": {
        "trust_proxy": 1
    },
    "server": {
        "type": "http", //protocol type
        "ip-mask":  "0.0.0.0", //ip mask
        "port": 5555
    },
    "body-parser": {
        "extended": false //json body parser flag
    }
}
//You can instantiate it without passing config
//a default config will be used instead
const server = new CommonServer(config);

//simply call start() to use your server
server.start()
.then( () => {
    console.log('Server is up!');
    return server.stop();
})
//stop
.then( () => {
    console.log('Server is down!');
});


//set routes to bo one of .get, .post, .put, .delete
server.route('your/path/')
    .get((__request) => {
        //to make success response use respondSuccess API
        let data = {hello: 'you!'}; 
        server.respondSuccess(__request, data, 'This is response message');
        //or
        //to make error response use respondSError API
        server.respondError(__request, 'This is error message');
    });

// response

```

### Response
This is what the browser or a client would get on success
```json
{   
    "date":"2018-03-29T19:53:58.806Z", //response date
    "error":false, //no error flag
    "code":200, //success code
    "path":"your/path/", //route path used above 
    "hostname":"localhost", //ip of server location
    "method":"GET", //method used one of GET | POST | PUT | DELETE
    "message":"This is response message", //success message used to set response in the example above
    "performance":"0h/0m/0s/0ms", //performance time between when the request was received and response made
    "data": { //response data object used in  server.respondSuccess() call
        "hello": "you!"
    }
}
```

This is what the browser or a client would get on error
```json
{   
    "date":"2018-03-29T19:53:58.806Z", //response date
    "error":true, //error flag
    "code":500, //error code, general HTTP status code
    "path":"your/path/", //route path used above 
    "hostname":"localhost",  //ip of server location
    "method":"GET", //method used, one of GET | POST | PUT | DELETE
    "message":"This is error message", //error message used to set response in the example above
    "performance":"0h/0m/0s/0ms" //performance time between when the request was received and response made
}
```


### Config

```json
{
    "cors":{
        "origin": "*",
        "optionsSuccessStatus": 200
    },
    "express": {
        "trust_proxy": 1
    },
    "server": {
        "type": "http",
        "ip-mask":  "0.0.0.0",
        "port": 5555
    },
    "body-parser": {
        "extended": false
    }
}

```