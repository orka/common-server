/**
 * Server module enables instance baed server route created to assign path of rest apis and 
 * server connection management
 * It is also highly configurable
 */
'use strict';
//
const LogModule = require('log-module');
const ConfModule = require('config-module');
const timestamp = require('timestamp');
//you can use either HTTPS server
const https = require('https');
//or HTTP
const http = require('http');
//server tool
const express = require('express');
//to allow cross origin
const cors = require('cors');
//need this to process the body arguments
const bodyParser = require('body-parser');
//
const defaultConfig = require('./config');

/**
 * Server class with a central configuration point
 */
module.exports = class Server extends LogModule {

    constructor(__config) {
        //!!! init Event Emitter
        super();

        this.__serverConfig = new ConfModule(defaultConfig, __config);

        //create express app
        this.app = express();
        //assign body parser
        this.app.use(bodyParser.urlencoded(this.__serverConfig.get('body-parser')));
        //
        this.app.use(bodyParser.json());
        //assign cors
        this.app.use(cors(this.__serverConfig.get('cors')));
        //
        this.app.set('trust_proxy', this.__serverConfig.get('express.trust_proxy'));
        //create server 
        let server = this.__serverConfig.get('server.type') == 'https' ? https : http;
        
        this.server = server.createServer(this.app);
        //track request performance by intercepting all requests adn capturing time
        this.app.use((__request, __res, next) => {
            __request.__requestTime = Date.now();
            next();
        });
    }

    /**
     * Assign apis to path
     * use .get .post .put .delete
     * @param {String} __path rout for apis  
     */
    route(__path) {
        return this.app.route(__path);
    }

    /**
     * Creates a standardized error response with information pertaining request itself 
     * and error message
     * @param {ServerRequest} ___request Server request object 
     * @param {String} ___message Opt. response message 
     */
    respondError(__request, __message) {
        //create standardized response
        let body = {
            error: true,
            statusCode: 500,
            date: new Date(),
            performance: timestamp.now(__request.__requestTime),
            ip: __request.ip,
            method: __request.method,
            hostname: __request.hostname,
            path: __request.path,
            message: __message || null,
        };
        //this status code will casts body into the error object of response
        __request.res.statusCode = 500;
        //send it
        __request.res.json(body);
        __request.res.end();
    }

    /**
     * Creates a standardized response with information pertaining request itself 
     * and attached payload as .data
     * @param {ServerRequest} ___request Server request object
     * @param {Object} ___data Opt, Response body, usually json
     * @param {String} ___message Opt. response message
     * @param {String|Object} ___performance Request processing time duration data
     */
    respondSuccess(__request, __data, __message, __performance) {
        //create standardized response
        let body = {
            error: false,
            statusCode: 200,
            date: new Date(),
            performance: timestamp.now(__request.__requestTime),
            ip: __request.ip,
            method: __request.method,
            hostname: __request.hostname,
            path: __request.path,
            message: __message || null,
            data: __data,
        };
        //
        __request.res.statusCode = 200;
        //send it
        __request.res.json(body);
        __request.res.end();
    }

    /**
     * Promisified start connection
     * Use to connect to server
     */
    start() {
        return new Promise((__resolve, __reject) => {
            try {
                let resolved = false;
                //callback to set this call as resolved
                let cb = (__resolved) => {
                    this.server.removeListener('listening', cb);
                    resolved = __resolved;
                    if (__resolved) {
                        __resolve(this.server);
                    } else {
                        __reject(this.server);
                    }
                }
                this.server.on('listening', (__event) => {
                    cb(true);
                });

                setTimeout(() => {
                    if (!resolved) {
                        cb(false);
                    }
                }, 1000);
                //
                this.server.listen(this.__serverConfig.get('server.port'), this.__serverConfig.get('server.ip-mask'));

            } catch (__err) {
                __reject(__err);
            }
        });
    }

    /**
     * Promisified stop connection
     * Use to stop server connection
     */
    stop() {
        return new Promise((__resolve, __reject) => {
            try {

                let resolved = false;
                //callback to set this call as resolved
                let cb = (__resolved) => {
                    this.server.removeListener('close', cb);
                    resolved = __resolved;
                    if (__resolved) {
                        __resolve(this.server);
                    } else {
                        __reject(this.server);
                    }
                }
                this.server.on('close', (__event) => {
                    cb(true);
                });

                setTimeout(() => {
                    if (!resolved) {
                        cb(false);
                    }
                }, 1000);
                //
                this.server.close();

            } catch (__err) {
                __reject(__err);
            }
        });
    }
}
